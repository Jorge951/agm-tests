import { Directive, Input, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { Position } from 'src/app/services/positions-generator-services';
import { GoogleMapsAPIWrapper, LatLngLiteral, PolylineManager } from '@agm/core';
import { Polyline, PolylineOptions, GoogleMap } from '@agm/core/services/google-maps-types';
/// <reference types="googlemaps" />

@Directive({
  selector: 'agm-tracert'
})
export class TracertDirective implements OnChanges, OnInit {

  @Input()
  public positions: Array<Position>

  private polyline: Polyline;

  private nativeMap: GoogleMap;

  constructor(private mapWrapper: GoogleMapsAPIWrapper, private polyManager: PolylineManager) { }

  ngOnInit(){
    this.mapWrapper.getNativeMap().then((map: GoogleMap) => {
      this.nativeMap = map;
    });
  }

  ngOnChanges(change: SimpleChanges){
    console.log('ngOnChange', change);
    if(change.positions.currentValue){
      this.tracert(<Array<Position>> change.positions.currentValue);
    }
  }

  updateChanges(){
    this.tracert(this.positions);
  }

  public removeTracert(){
    if(this.polyline)
      this.polyline.setMap(null);
  }

  public tracert(positions: Array<Position>){
    this.removeTracert();

    console.log('onTracert', positions);
    let latslangs: Array<LatLngLiteral> = new Array<LatLngLiteral>();

    positions.forEach((position) => {
      latslangs.push({
        lat: position.latitud,
        lng: position.longitud
      } as LatLngLiteral)
    });

    this.mapWrapper.createPolyline({
      path: latslangs,
      clickable: false,
      map: this.nativeMap,
      strokeColor: '#FF0000',
      strokeOpacity: 1,
      icons: [{
        icon: {
          path: 'M 0,-1 0,1',
          scale: 4,
          strokeColor: '#0000A0',
        },
        repeat: '27px'
      }] 
    } as PolylineOptions).then((polyline: Polyline) => {
      console.log('polyline created', polyline);
      this.polyline = polyline;
    })
  }

}
