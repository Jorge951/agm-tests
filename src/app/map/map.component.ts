import { TracertDirective } from './directives/tracert.directive';
import { BBox } from '@turf/turf';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PositionsGeneratorService, Position } from '../services/positions-generator-services';
import { LatLngLiteral } from '@agm/core';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @ViewChild(TracertDirective) public tracert: TracertDirective;

  public positions: Array<Position> = new Array<Position>();

  public latitud: number;
  public longitud: number;

  constructor(private posService: PositionsGeneratorService) { }

  ngOnInit() {}  

  public addMarker(quanty: number, box: BBox = [18.761900674064723, -103.0008000630221, 27.62067286084142, -97.43685927245718]): void{
    this.posService.generatePositions(quanty, box).forEach(position => this.positions.push(position));

    this.tracert.updateChanges();
  }

  public clearMarkers(){
    while(this.positions.length > 0)
      this.positions.shift();

    this.tracert.updateChanges();
  }

  public centerChange(latlng: LatLngLiteral){
    this.latitud = latlng.lat;
    this.longitud = latlng.lng;
  }

}
