import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';

import { AgmCoreModule } from '@agm/core';
import { MapControllersComponent } from './map-controllers/map-controllers.component';
import { TracertDirective } from './map/directives/tracert.directive'

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    MapControllersComponent,
    TracertDirective
  ],
  imports: [
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyCSsis-BO_CBsWAic46ZEWd1N98W1kJPNY"
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
