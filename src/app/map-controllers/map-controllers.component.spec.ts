import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MapControllersComponent } from './map-controllers.component';

describe('MapControllersComponent', () => {
  let component: MapControllersComponent;
  let fixture: ComponentFixture<MapControllersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MapControllersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapControllersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
