import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-map-controllers',
  templateUrl: './map-controllers.component.html',
  styleUrls: ['./map-controllers.component.scss']
})
export class MapControllersComponent implements OnInit {

  @Output()
  public addMarker: EventEmitter<number> = new EventEmitter<number>();

  @Output()
  public clearMarkers: EventEmitter<void> = new EventEmitter<void>();

  @Input()
  public positions: Array<Position>;

  constructor() { }

  ngOnInit() {}

  public addMarkerPressed(quanty?: number){
    this.addMarker.emit(!quanty ? 1 : quanty);
  }

  public clearMarkersPressed(){
    this.clearMarkers.emit();
  }
}
